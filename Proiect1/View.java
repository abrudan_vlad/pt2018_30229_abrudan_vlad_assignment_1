import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.MouseWheelEvent;
import java.nio.channels.Pipe;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.Caret;
public class View extends JFrame {

private JLabel l1=new JLabel("Primul polinom");
private JLabel l2=new JLabel("Al doilea polinom");
private JLabel l3=new JLabel("Rezultat");
private JButton b1 =new JButton("Derivare");
private JButton b2 =new JButton("Integrare");
private JButton b3 =new JButton("Adunare");
private JButton b4 =new JButton("Scadere");
private JButton b5 =new JButton("Inmultire");
private JButton b6 =new JButton("Impartire");
private JTextField tf1=new JTextField();
private JTextField tf2=new JTextField();
private JTextArea ta1=new JTextArea();
public View() {
	JPanel p1=new JPanel();
	JPanel p2=new JPanel();
    JPanel p3=new JPanel();
	JPanel p4=new JPanel();
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setSize(new Dimension(300,180));
	p1.setLayout(new BorderLayout());
	p2.setLayout(new FlowLayout());
	p2.add(b1);
	p2.add(b2);
	p2.add(b3);
	p2.add(b4);
	p2.add(b5);
	p2.add(b6);
    p3.setLayout(new BoxLayout(p3,BoxLayout.Y_AXIS));
    p3.setBorder(BorderFactory.createEmptyBorder(50, 50, 50, 50));
    p3.add(l1);
    p3.add(tf1);
    p3.add(l2);
    p3.add(tf2);
    p4.setLayout(new BoxLayout(p4,BoxLayout.Y_AXIS));
    p4.setBorder(BorderFactory.createEmptyBorder(50, 100, 300, 100));
    p4.add(l3);
    p4.add(ta1);
	p1.add(p2,BorderLayout.SOUTH);
	p1.add(p3,BorderLayout.NORTH);
	p1.add(p4,BorderLayout.WEST);
	this.setSize(500,500);
	this.setLocation(400, 200);
	this.setContentPane(p1);
	this.setVisible(true);
}

public JButton getButtonAdunare() {
	return b3;
}
public JButton getButtonScadere() {
	return b4;
}
public JButton getButtonInmultire() {
	return b5;
}
public JButton getButtonDerivare() {
	return b1;
}
public JButton getButtonIntegrare() {
	return b2;
}
public JButton getButtonImpartire() {
	return b6;
}

public JTextField getText1() {
	return tf1;
}
public JTextField getText2() {
	return tf2;
}
public JTextArea getArea() {
	return ta1;
}
}
