import java.awt.event.ActionEvent;
import java.util.regex.*;
import java.awt.event.ActionListener;

public class Controller {
	private View view= new View();
	private String pat="([+-]?\\d+)?([+-]?x)(\\^(\\d+))?|([+-]?\\d+)";
 
	public Controller() {
		view.getButtonAdunare().addActionListener(new ListenerSuma());
		view.getButtonScadere().addActionListener(new ListenerScadere());
		view.getButtonDerivare().addActionListener(new ListenerDerivare());
		view.getButtonIntegrare().addActionListener(new ListenerIntegrare());
		view.getButtonInmultire().addActionListener(new ListenerInmultire());
	}
	public void comparare(String pat1, String deComparat, Polinom p) {
		Pattern reg=Pattern.compile(pat1);
		Matcher m=reg.matcher(deComparat);
		int coef=0;
		int grad=0;
		while(m.find()) {
			if(m.group(2)==null) 
			{
				grad=0;
				coef=Integer.parseInt(m.group(5));
			}
			else {  
				if(m.group(2).contains("x")) {
					if(m.group(1)==null)
						coef=1;
					else
					coef=Integer.parseInt(m.group(1));
					if(m.group(4)==null)
						grad=1;
					else
						grad=Integer.parseInt(m.group(4));
				}
				if(m.group(2).contains("-x"))
				{
					coef=-1;
					if(m.group(4)==null)
						grad=1;
					else
						grad=Integer.parseInt(m.group(4));
				}
				if(m.group(2).contains("+x")) {
					coef=1;
					if(m.group(4)==null)
						grad=1;
					else
						grad=Integer.parseInt(m.group(4));
				}
	            }
			p.addMonom(new Monom(coef, grad));
				}
	}
	
class ListenerSuma implements ActionListener{

	public void actionPerformed(ActionEvent e) {
		Polinom p1=new Polinom();
		Polinom p2=new Polinom();
		comparare(pat, view.getText1().getText(), p1);
		comparare(pat, view.getText2().getText(), p2);
		Polinom p3=p1.adunare(p2);
		p3.getMonoame().sort(new Monom());
		String a=p3.afisarePolinom();
		view.getArea().setText(a);
		
	}
	}

class ListenerScadere implements ActionListener{
	public void actionPerformed(ActionEvent e) {
	
	Polinom p1=new Polinom();
	Polinom p2=new Polinom();
	comparare(pat, view.getText1().getText(), p1);
	comparare(pat, view.getText2().getText(), p2);
	Polinom p3=p1.scadere(p2);
	p3.getMonoame().sort(new Monom());
	String a=p3.afisarePolinom();
		view.getArea().setText(a);
	}
}
class ListenerDerivare implements ActionListener{
		public void actionPerformed(ActionEvent e) {
		Polinom polinom=new Polinom();
		comparare(pat, view.getText1().getText(), polinom);
		polinom.derivare();
		polinom.getMonoame().sort(new Monom());
		String a=polinom.afisarePolinom();
		view.getArea().setText(a);
		}
	}
class ListenerIntegrare implements ActionListener{
	public void actionPerformed(ActionEvent e) {
	Polinom p=new Polinom();
	comparare(pat, view.getText1().getText(), p);
	p.integrare();
	p.getMonoame().sort(new Monom());
	String a=p.afisarePolinomR();
	view.getArea().setText(a);
	}
}
 
class ListenerInmultire implements ActionListener{
	public void actionPerformed(ActionEvent e) {
		Polinom p1=new Polinom();
		Polinom p2=new Polinom();
		Polinom p3=new Polinom();
		comparare(pat, view.getText1().getText(), p1);
		comparare(pat, view.getText2().getText(), p2);
		p3=p1.inmultire(p2);
		p3.getMonoame().sort(new Monom());
		String a=p3.afisarePolinom();
		view.getArea().setText(a);
	}
	
}
}


