import static org.junit.Assert.assertEquals;

import junit.framework.*;
public class TestClass extends TestCase {
	 Polinom p1=new Polinom();
	 Polinom p2=new Polinom();	
public void setUp() {
	 p1.addMonom(new Monom(3, 2));
	 p1.addMonom(new Monom(2, 0));
	 p2.addMonom(new Monom(4,2));
	 p2.addMonom(new Monom(1,0));
	}
public void testSuma() {
	Polinom a=p1.adunare(p2);
	String s=a.afisarePolinom();
	assertEquals("7x^2+3", s);
}

public void testScadere() {
	Polinom a=p1.scadere(p2);
	String s=a.afisarePolinom();
	assertEquals("-x^2+1", s);
}

public void testInmultire() {
	Polinom a=p1.inmultire(p2);
	String s=a.afisarePolinom();
	assertEquals("12x^4+11x^2+2", s);
	
}

public void testDerivare() {
	p1.derivare();
	String s=p1.afisarePolinom();
	assertEquals("6x", s);
	
}

public void testIntegrare() {
	p1.integrare();
	String s=p1.afisarePolinomR();
	assertEquals("x^3+2.0x^1", s);
}

public void tearDown() {
	p1=null;
	p2=null;
}

}
