import java.util.Comparator;

public class Monom implements Comparator<Monom>{
private int coeficient;
private int grad;
private double coeficientR;

public Monom(){
	coeficient=0;
	grad=0;
}

public Monom(int coeficient, int grad) {
	this.coeficient=coeficient;
	this.grad=grad;
}

public int getGrad()
{
	return grad;
}

public int getCoeficient()
{
	return coeficient;
}

public double getCoeficientR() {
	return coeficientR;
}

public void setCoeficientR(double a){
	this.coeficientR=a;
}

public void setCoeficient(int coef) {
	
	coeficient=coef;
}

public void setGrad(int grad) {
	
	this.grad=grad;
}

public void adunare(Monom a) {
		this.coeficient+=a.coeficient;
}

public Monom inmultire(Monom a) {
	Monom b=new Monom();
b.coeficient=this.coeficient*a.coeficient;
b.grad=this.grad+a.grad;
return b;
}

public void derivare() {
	this.coeficient*=grad;
	this.grad--;
}

public void integrare() {
	this.grad++;
	coeficientR=(double)coeficient/(double)(this.grad);
}

public void negare() {
	this.coeficient=-coeficient; 
}
@Override
public int compare(Monom a, Monom b) {
	if(a.getGrad()<b.getGrad())
		return 1;
	else
		if(a.getGrad()==b.getGrad())
			return 0;
		else {
			return -1;
		}
	
}



}
