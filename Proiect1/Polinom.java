import java.util.ArrayList;
public class Polinom {
private ArrayList<Monom> monoame=new ArrayList<Monom>();


public void  addMonom(Monom a)  {
	monoame.add(a);
}
public ArrayList<Monom> getMonoame(){
	return monoame;
}

public Polinom adunare(Polinom a) {
	Polinom b=new Polinom();
 for (Monom monom : this.monoame) {
	 int ok=0;
	for (Monom monom1 : a.monoame) {
		if(monom.getGrad()==monom1.getGrad())
		{
			monom.adunare(monom1);
			b.addMonom(monom);
			ok=1;
		}		
	}
	if(ok==0)
		b.addMonom(monom);
}
 for (Monom monom : a.monoame) {
	 int ok1=0;
	 for (Monom monom1 : monoame) {
		if (monom.getGrad()==monom1.getGrad()) {
			ok1=1;		
		}
	    }
	if(ok1==0)
		b.addMonom(monom);
    }
 return b;
}

public Polinom scadere(Polinom a) {
	Polinom b=new Polinom();
 for (Monom monom : a.monoame) {
      monom.negare();
      }
 b=this.adunare(a);
 return b;
 }

public void derivare() {
for (Monom monom : this.monoame) {
	monom.derivare();
    }
}

public void integrare() {
	for (Monom monom : this.monoame) {
	monom.integrare();	
	}
}

public Polinom inmultire(Polinom a) {
Polinom b=new Polinom();
 for (Monom monom : this.monoame) {
	 Polinom c=new Polinom();
	 for (Monom monom1 : a.monoame) {
		 c.addMonom(monom.inmultire(monom1)); 
	}
	b=b.adunare(c);
}
	return b;
}

public String afisarePlus() {
 String s="";
 for (Monom monom : monoame) {
	 if( monom.getCoeficient()==0)
		 s=s.concat("");
	 else
		 if(monom.getCoeficient()==1)
			 if(monom.getGrad()==1)
	           s=s.concat("x+");
	           else
	        	   if(monom.getGrad()==0)
	        		   s=s.concat("1+");
	        	   else
	        	   s=s.concat("x^"+monom.getGrad()+"+");
		 else {
			if(monom.getCoeficient()>1)
				if(monom.getGrad()==1)
			           s=s.concat(monom.getCoeficient()+"x+");
			           else
			        	   if(monom.getGrad()==0)
			        		   s=s.concat(monom.getCoeficient()+"+");
			        	   else
			        	   s=s.concat(monom.getCoeficient()+"x^"+monom.getGrad()+"+");
		}
 }
 return s;
}
public String afisareMinus() {
	 String s="";
	 for (Monom monom : monoame) {
		 if( monom.getCoeficient()==0)
			 s=s.concat("");
		 else
			 if(monom.getCoeficient()==-1)
				 if(monom.getGrad()==1)
		           s=s.concat("-x");
		           else
		        	   if(monom.getGrad()==0)
		        		   s=s.concat("-1");
		        	   else
		        	   s=s.concat("-x^"+monom.getGrad()+"");
			 else {
				if(monom.getCoeficient()<1)
					if(monom.getGrad()==1)
				           s=s.concat(monom.getCoeficient()+"x");
				           else
				        	   if(monom.getGrad()==0)
				        		   s=s.concat(monom.getCoeficient()+"");
				        	   else
				        	   s=s.concat(monom.getCoeficient()+"x^"+monom.getGrad()+"");
			}
	 }
	 return s;
	}
public String afisarePolinom() {
	String minus=afisareMinus();
	String plus=afisarePlus();
	String s="";
	String r="";
	if(minus.isEmpty() && plus.isEmpty())
		r="0";
	else
	if(minus.isEmpty()) {
		s=s.concat(plus);
		r=s.substring(0, s.length()-1);
	}
	else
		if(plus.isEmpty()) {
			s=s.concat(minus);
			r=s;
			}
		else {
			s=s.concat(minus+"+"+plus);
	        r=s.substring(0, s.length()-1);
		}
	return r;
}
public String afisarePolinomR() {
String s="";
	for (Monom monom : monoame) {
		s=s.concat(monom.getCoeficientR()+"x^"+monom.getGrad()+"+");
	}
	s=s.replace("+-", "-");
	s=s.replace("1.0","");
	if (!s.isEmpty())
		s=s.substring(0, s.length()-1);
	return s;
}
}
